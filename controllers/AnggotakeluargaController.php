<?php

namespace app\controllers;

use Yii;
use app\models\Anggotakeluarga;
use app\models\AnggotakeluargaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AnggotakeluargaController implements the CRUD actions for Anggotakeluarga model.
 */
class AnggotakeluargaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Anggotakeluarga models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AnggotakeluargaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Anggotakeluarga model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Anggotakeluarga model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Anggotakeluarga();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Kode_Peserta]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Anggotakeluarga model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Kode_Peserta]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Anggotakeluarga model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Anggotakeluarga model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Anggotakeluarga the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Anggotakeluarga::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
          //Export Excel
    public function actionExportExcel() {
        $searchModel = new AnggotakeluargaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 0;

        $style = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $styleArrayRightThin = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftThin = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftMedium = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayRightMedium = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayBottomMedium = array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayTopMedium = array(
            'borders' => array(
                'top' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $template = Yii::getAlias('@hscstudio/export') . '/templates/phpexcel/pesertacitybike.xlsx';
        $objPHPExcel = $objReader->load($template);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $baseRow = 3; // line 2
        $no = 1;
        foreach ($dataProvider->getModels() as $transaksi) {
            // Year
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $baseRow, $transaksi->citybike->Nama_Peserta);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $baseRow, $transaksi->citybike->Nama_Peserta)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
            // Month
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $baseRow, $transaksi->Nama);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $baseRow, $transaksi->Nama)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            // Date
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $baseRow, $transaksi->Usia);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $baseRow, $transaksi->Usia)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
  

            // Type
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $baseRow, $transaksi->Jenis_Kelamin);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $baseRow, $transaksi->Jenis_Kelamin)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            
                // Category
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $baseRow, $transaksi->Ukuran_Jersey);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $baseRow, $transaksi->Ukuran_Jersey)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

             $objPHPExcel->getActiveSheet()->setCellValue('F' . $baseRow, $transaksi->Jenis_Sepeda);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $baseRow, $transaksi->Jenis_Sepeda)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            
             $objPHPExcel->getActiveSheet()->setCellValue('G' . $baseRow, $transaksi->field1);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $baseRow, $transaksi->field1)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

           
            $baseRow++;
            $no++;
        }
   

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="peserta_city_bike.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }
}
