<?php

namespace app\controllers;

use Yii;
use app\models\Pesertafutsal;
use app\models\PesertafutsalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PesertafutsalController implements the CRUD actions for Pesertafutsal model.
 */
class PesertafutsalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pesertafutsal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PesertafutsalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pesertafutsal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pesertafutsal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pesertafutsal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Kode_Peserta]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pesertafutsal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Kode_Peserta]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pesertafutsal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pesertafutsal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pesertafutsal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pesertafutsal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
          //Export Excel
    public function actionExportExcel() {
        $searchModel = new PesertafutsalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 0;

        $style = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $styleArrayRightThin = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftThin = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftMedium = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayRightMedium = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayBottomMedium = array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayTopMedium = array(
            'borders' => array(
                'top' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $template = Yii::getAlias('@hscstudio/export') . '/templates/phpexcel/pesertafutsal.xlsx';
        $objPHPExcel = $objReader->load($template);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $baseRow = 3; // line 2
        $no = 1;
        foreach ($dataProvider->getModels() as $transaksi) {
            // Year
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $baseRow, $transaksi->futsal->Nama_Team);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $baseRow, $no)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
            // Month
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $baseRow, $transaksi->Nama);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $baseRow, $transaksi->Nama)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            // Date
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $baseRow, $transaksi->NIK);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $baseRow, $transaksi->NIK)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
  

            // Type
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $baseRow, $transaksi->Jenis_Kelamin);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $baseRow, $transaksi->Jenis_Kelamin)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            
                // Category
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $baseRow, $transaksi->No_HP);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $baseRow, $transaksi->No_HP)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            
                  // Category
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $baseRow, $transaksi->Keterangan);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $baseRow, $transaksi->Keterangan)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

           
            $baseRow++;
            $no++;
        }
   

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="pesertafutsal.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }
}
