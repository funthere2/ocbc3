<?php

namespace app\controllers;

use Yii;
use app\models\Citybike;
use app\models\CitybikeSearch;
use app\models\Anggotakeluarga;
use app\models\AnggotakeluargaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * CitybikeController implements the CRUD actions for Citybike model.
 */
class CitybikeController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Citybike models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CitybikeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Citybike model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        //detail Sales
        $detailQuery = Anggotakeluarga::find()
                ->where(['Kode_Citybike' => $id])
        ;
        $detailDataProvider = new ActiveDataProvider([
            'query' => $detailQuery,
        ]);

        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'detailDataProvider' => $detailDataProvider
        ]);
    }

    /**
     * Creates a new Citybike model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Citybike();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Kode_Citybike]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionRegister() {
        $model = new Citybike();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->sendmail($model->Kode_Citybike);
            Yii::$app->session->setFlash('success', Yii::$app->params['pendaftaransukses']);
            return $this->redirect(['/site/index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionRegisterwithfamily() {
        $model = new Citybike();
        $details = [ new Anggotakeluarga];

        // proses isi post variable 
        if ($model->load(Yii::$app->request->post())) {
            $details = Model::createMultiple(Anggotakeluarga::classname());
            Model::loadMultiple($details, Yii::$app->request->post());

            // assign default transaction_id
            foreach ($details as $detail) {
                $detail->Kode_Citybike = 0;
            }

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                                ActiveForm::validateMultiple($details), ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid1 = $model->validate();
            $valid2 = Model::validateMultiple($details);
            $valid = $valid1 && $valid2;

            // jika valid, mulai proses penyimpanan
            if ($valid) {
                // mulai database transaction
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    // simpan master record                   
                    if ($flag = $model->save(false)) {
                        // simpan details record
                        foreach ($details as $detail) {
                            $detail->Kode_Citybike = $model->Kode_Citybike;
                            if (!($flag = $detail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        // sukses, commit database transaction
                        // kemudian tampilkan hasilnya
                        $transaction->commit();
                        $this->sendmail($model->Kode_Citybike);
                        Yii::$app->session->setFlash('success', Yii::$app->params['pendaftaransukses']);
                        return $this->redirect(['/site/index']);
                    } else {
                        return $this->render('createwithfamily', [
                                    'model' => $model,
                                    'details' => $details,
                        ]);
                    }
                } catch (Exception $e) {
                    // penyimpanan galga, rollback database transaction
                    $transaction->rollBack();
                    throw $e;
                }
            } else {
                return $this->render('create', [
                            'model' => $model,
                            'details' => $details,
                            'error' => 'valid1: ' . print_r($valid1, true) . ' - valid2: ' . print_r($valid2, true),
                ]);
            }
        } else {
            // inisialisai id 
            // diperlukan untuk form master-detail
            $model->Kode_Citybike = 0;
            // render view
            return $this->render('createwithfamily', [
                        'model' => $model,
                        'details' => $details,
            ]);
        }
    }

    public function actionRegisterawal() {
        return $this->render('registerawal');
    }

    /**
     * Updates an existing Citybike model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Kode_Citybike]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Citybike model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /* public function actionDelete($id)
      {
      $this->findModel($id)->delete();

      return $this->redirect(['index']);
      } */

    public function actionDelete($id) {
        $model = $this->findModel($id);
        $details = $model->transactionDetails;
        // mulai database transaction
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // pertama, delete semua detail records
            foreach ($details as $detail) {
                $detail->delete();
            }
            // kemudian, delete master record
            $model->delete();
            // sukses, commit transaction
            $transaction->commit();
        } catch (Exception $e) {
            // gagal, rollback database transaction
            $transaction->rollBack();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Citybike model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Citybike the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Citybike::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //Export Excel
    public function actionExportExcel() {
        $searchModel = new CitybikeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 0;

        $style = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $styleArrayRightThin = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftThin = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftMedium = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayRightMedium = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayBottomMedium = array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayTopMedium = array(
            'borders' => array(
                'top' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayBottomThin = array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $template = Yii::getAlias('@hscstudio/export') . '/templates/phpexcel/citybike.xlsx';
        $objPHPExcel = $objReader->load($template);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $baseRow = 3; // line 2
        $no = 1;
        foreach ($dataProvider->getModels() as $transaksi) {
            // Year
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $baseRow, $no);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $baseRow, $no)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            // Month
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $baseRow, $transaksi->Nama_Peserta);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $baseRow, $transaksi->Nama_Peserta)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


            $objPHPExcel->getActiveSheet()->setCellValue('C' . $baseRow, $transaksi->Jenis_Kelamin);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $baseRow, $transaksi->Jenis_Kelamin)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objPHPExcel->getActiveSheet()->setCellValue('D' . $baseRow, $transaksi->Email_Peserta);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $baseRow, $transaksi->Email_Peserta)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objPHPExcel->getActiveSheet()->setCellValue('E' . $baseRow, $transaksi->No_HP);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $baseRow, $transaksi->No_HP)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objPHPExcel->getActiveSheet()->setCellValue('F' . $baseRow, $transaksi->NIK);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $baseRow, $transaksi->NIK)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            // Type
            // Category
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $baseRow, $transaksi->Kategori_City_Bike);
            $objPHPExcel->getActiveSheet()->getStyle('G' . $baseRow, $transaksi->Kategori_City_Bike)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


            // Category
            $objPHPExcel->getActiveSheet()->setCellValue('H' . $baseRow, $transaksi->Ukuran_Jersey);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $baseRow, $transaksi->Ukuran_Jersey)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objPHPExcel->getActiveSheet()->setCellValue('I' . $baseRow, $transaksi->Nama_Anggota_Keluarga);
            $objPHPExcel->getActiveSheet()->getStyle('I' . $baseRow, $transaksi->Nama_Anggota_Keluarga)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objPHPExcel->getActiveSheet()->setCellValue('J' . $baseRow, $transaksi->Keterangan);
            $objPHPExcel->getActiveSheet()->getStyle('J' . $baseRow, $transaksi->Keterangan)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);



            $baseRow++;
            $no++;
        }



        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="peserta_city_bike.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }

    //Export Excel
    public function actionExportForm($id) {

        $header = $this->findModel($id);

        $detail = $this->findDetails($id);


        $style = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $styleArrayRightThin = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftThin = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftMedium = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayRightMedium = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayBottomMedium = array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayTopMedium = array(
            'borders' => array(
                'top' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayBottomThin = array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $template = Yii::getAlias('@hscstudio/export') . '/templates/phpexcel/citybikeform.xlsx';
        $objPHPExcel = $objReader->load($template);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        $objPHPExcel->getActiveSheet()->setCellValue('B2', $header->Nama_Peserta);


        $objPHPExcel->getActiveSheet()->setCellValue('B4', $header->Email_Peserta);


        $objPHPExcel->getActiveSheet()->setCellValue('B6', $header->No_HP);


        $objPHPExcel->getActiveSheet()->setCellValue('B8', $header->NIK);


        $objPHPExcel->getActiveSheet()->setCellValue('B10', $header->Kategori_City_Bike);


        $objPHPExcel->getActiveSheet()->setCellValue('B12', $header->Ukuran_Jersey);


        $objPHPExcel->getActiveSheet()->setCellValue('B14', $header->Nama_Anggota_Keluarga);

        $objPHPExcel->getActiveSheet()->setCellValue('B16', $header->Jenis_Kelamin);

        $objPHPExcel->getActiveSheet()->setCellValue('B18', $header->Keterangan);


        $baseRowDetail = 22; // line 2
        foreach ($detail->getModels() as $transaksi) {
            // Year
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $baseRowDetail, $transaksi->Nama);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $baseRowDetail)->applyFromArray($styleArrayLeftThin);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $baseRowDetail)->applyFromArray($styleArrayRightThin);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $baseRowDetail)->applyFromArray($styleArrayBottomThin);


            // Month
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $baseRowDetail, $transaksi->Usia);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $baseRowDetail)->applyFromArray($styleArrayLeftThin);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $baseRowDetail)->applyFromArray($styleArrayRightThin);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $baseRowDetail)->applyFromArray($styleArrayBottomThin);
            // Date
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $baseRowDetail, $transaksi->Jenis_Kelamin);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $baseRowDetail)->applyFromArray($styleArrayLeftThin);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $baseRowDetail)->applyFromArray($styleArrayRightThin);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $baseRowDetail)->applyFromArray($styleArrayBottomThin);

            $objPHPExcel->getActiveSheet()->setCellValue('D' . $baseRowDetail, $transaksi->Ukuran_Jersey);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $baseRowDetail)->applyFromArray($styleArrayLeftThin);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $baseRowDetail)->applyFromArray($styleArrayRightThin);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $baseRowDetail)->applyFromArray($styleArrayBottomThin);


            $objPHPExcel->getActiveSheet()->setCellValue('E' . $baseRowDetail, $transaksi->Jenis_Sepeda);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $baseRowDetail)->applyFromArray($styleArrayLeftThin);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $baseRowDetail)->applyFromArray($styleArrayRightThin);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $baseRowDetail)->applyFromArray($styleArrayBottomThin);

            $objPHPExcel->getActiveSheet()->setCellValue('F' . $baseRowDetail, $transaksi->field1);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $baseRowDetail)->applyFromArray($styleArrayLeftThin);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $baseRowDetail)->applyFromArray($styleArrayRightThin);
            $objPHPExcel->getActiveSheet()->getStyle('F' . $baseRowDetail)->applyFromArray($styleArrayBottomThin);






            $baseRowDetail++;
            $no++;
        }






        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="citybikeform.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }

    public function actionUpdatewithfamily($id) {
        $model = $this->findModel($id);
        $details = $model->transactionDetails;
        if ($model->load(Yii::$app->request->post())) {
            $oldIDs = ArrayHelper::map($details, 'Kode_Peserta', 'Kode_Peserta');
            $details = Model::createMultiple(Anggotakeluarga::classname(), $details);
            Model::loadMultiple($details, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($details, 'Kode_Peserta', 'Kode_Peserta')));
            // assign default transaction_id
            foreach ($details as $detail) {
                $detail->Kode_Citybike = $model->Kode_Citybike;
            }
            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                                ActiveForm::validateMultiple($details), ActiveForm::validate($model)
                );
            }
            // validate all models
            $valid1 = $model->validate();
            $valid2 = Model::validateMultiple($details);
            $valid = $valid1 && $valid2;
            // jika valid, mulai proses penyimpanan
            if ($valid) {
                // mulai database transaction
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    // simpan master record                   
                    if ($flag = $model->save(false)) {
                        // delete dahulu semua record yang ada
                        if (!empty($deletedIDs)) {
                            Anggotakeluarga::deleteAll(['Kode_Peserta' => $deletedIDs]);
                        }
                        // simpan details record
                        foreach ($details as $detail) {
                            $detail->Kode_Citybike = $model->Kode_Citybike;
                            if (!($flag = $detail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        // sukses, commit database transaction
                        // kemudian tampilkan hasilnya
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->Kode_Citybike]);
                    }
                } catch (Exception $e) {
                    // penyimpanan galga, rollback database transaction
                    $transaction->rollBack();
                    throw $e;
                }
            } else {
                return $this->render('create', [
                            'model' => $model,
                            'details' => $details,
                            'error' => 'valid1: ' . print_r($valid1, true) . ' - valid2: ' . print_r($valid2, true),
                ]);
            }
        }
        // render view
        return $this->render('updatewithfamily', [
                    'model' => $model,
                    'details' => (empty($details)) ? [new Anggotakeluarga] : $details
        ]);
    }

    protected function findDetails($id) {
        $detailModel = new AnggotakeluargaSearch();
        return $detailModel->search(['AnggotakeluargaSearch' => ['Kode_Citybike' => $id]]);
    }

    public function sendmail($id) {
        $model = Citybike::findOne($id);
        $detail = Anggotakeluarga::find()->where(['Kode_Citybike' => $id])->all();

        if ($model) {
            $subject = "Konfirmasi Pendaftaran Gowes Bareng #49TahunIndosatOoredoo";
            $sender = Yii::$app->params['senderMail'];
            $body = "Yth Bapak/Ibu <b>" . $model->Nama_Peserta . "</b><br><br>";
            $body .= "Pendaftaran anda pada event Gowes Bareng #49TahunIndosatOoredoo sudah berhasil. Mohon segera lakukan transfer biaya pendaftaran <b>Rp 49.000/peserta</b> ke akun Dompetku atas nama <b>Irene Angelina Sugianto</b> dengan no hp <b>08551323623</b> paling lambat tanggal 31 Oktober 2016. Bukti transfer kirim ke irene.sugianto@indosatooredoo.com.<br><br>";

            $body .= "Event tersebut akan dilaksanakan pada:<br>";
            $body .= "<p>Waktu: <b>13 November 2016 | 06.00 sampai 13.00</b><br>";
            $body .= "Tempat: <b> Start dan Finish di KPPTI</b><br><br>";
            //  $body .= "Jarak : <b>49 KM & 4,9 KM </b>(Jalur akan diberitahukan kemudian <br>";
            //$body .= "Pendaftaran: <b>ditutup 31 Okt 2016, pukul 11.00 WIB</b><br><br>";

            $body .= "<b>Data Pendaftaran:</b><ul>";
            $body .= "<li>Nama Peserta  : <b>" . $model->Nama_Peserta . "</b></li>";
            $body .= "<li>Jenis Kelamin : <b>" . $model->Jenis_Kelamin . "</b></li>";
            $body .= "<li>NIK           : <b>" . $model->NIK . "</b></li>";
            $body .= "<li>Departemen    : <b>" . $model->Nama_Anggota_Keluarga . "</b></li>";
            $body .= "<li>Email Peserta : <b>" . $model->Email_Peserta . "</b></li>";
            $body .= "<li>No HP         : <b>" . $model->No_HP . "</b></li>";
            $body .= "<li>Jarak Tempuh  : <b>" . $model->Kategori_City_Bike . "</b></li>";
            $body .= "<li>Ukuran Jersey : <b>" . $model->Ukuran_Jersey . "</b></li>";
            $body .= "<li>Jenis Sepeda  : <b>" . $model->Keterangan . "</b></li></ul>";

            if ($detail) {
                $body .= "<b>Anggota keluarga yang ikut serta:</b><br><br>";
                $body .= "<table border='0.5'>";
                $body .= "<tr>";
                $body .= "<th>No</th>";
                $body .= "<th>Nama</th>";
                $body .= "<th>Usia</th>";
                $body .= "<th>Jenis Kelamin</th>";
                $body .= "<th>Ukuran Jersey</th>";
                $body .= "<th>Jarak Tempuh</th>";
                $body .= "<th>Jenis Sepeda</th>";
                $body .= "</tr>";
                $no = 0;
                foreach ($detail as $item) {

                    $body .= "<tr>";
                    $body .= "<td>" . ++$no . "</td>";
                    $body .= "<td>" . $item->Nama . "</td>";
                    $body .= "<td>" . $item->Usia . "</td>";
                    $body .= "<td>" . $item->Jenis_Kelamin . "</td>";
                    $body .= "<td>" . $item->Ukuran_Jersey . "</td>";
                    $body .= "<td>" . $item->field1 . "</td>";
                    $body .= "<td>" . $item->Jenis_Sepeda . "</td>";
                    $body .= "</tr>";
                }
                $body .= "</table><br><br>";
            }

            $body .= "<b>Informasi Tambahan:</b><ul>";
            $body .= "<li>Rute akan diberitahukan kemudian</li>";
            $body .= "<li>Peserta membawa sepedanya masing-masing untuk mengikuti fun bike. Panitia tidak menyediakan peminjaman/ penyewaan sepeda</li>";
            $body .= "<li>Pembagian Race Pack: 11 November 2016. Harap membawa bukti transfer dan email ini saat pengambilan.</li>";
            $body .= "<li>Untuk informasi lebih lanjut hubungi Achmadi Prasetyo: achmadi.prasetyo@indosatooredoo.com / 08557890111</li></ul>";

            $body .= "<br><br>Regards,";

            $headers = "From: <{$sender}>\r\n" .
                    "Reply-To: {$sender}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Transfer-Encoding: 8bit\r\n" .
                    "Content-Type: text/html; charset=ISO-8859-1\r\n";

            mail($model->Email_Peserta, $subject, $body, $headers);
        } 
    }

}
