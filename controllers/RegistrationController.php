<?php

namespace app\controllers;

use Yii;
use app\models\Registration;
use app\models\RegistrationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RegistrationController implements the CRUD actions for Registration model.
 */
class RegistrationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Registration models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('pilihkelas');
    }
    public function actionIsidata()
    {
        return $this->render('isidata');
    }
    public function actionDatatiket()
    {
        return $this->render('datatiket');
    }
    public function actionMetodebayar()
    {
        return $this->render('metodepembayaran');
    }
    public function actionEmailsukses()
    {
        return $this->render('emailsukses');
    }
    public function actionLoginsukses()
    {
        return $this->render('loginsukses');
    }
    public function actionUbahkelas()
    {
        return $this->render('ubahkelas');
    }
    public function actionEmailperubahankelas()
    {
        return $this->render('emailperubahankelas');
    }
    public function actionEticket()
    {
        return $this->render('eticket');
    }
    public function actionEmailevent()
    {
        return $this->render('emailevent');
    }

}
