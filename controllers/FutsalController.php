<?php

namespace app\controllers;

use Yii;
use app\models\Futsal;
use app\models\FutsalSearch;
use app\models\Pesertafutsal;
use app\models\PesertafutsalSearch;
use app\models\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * FutsalController implements the CRUD actions for Futsal model.
 */
class FutsalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Futsal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FutsalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Futsal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        
          //detail Sales
        $detailQuery = Pesertafutsal::find()
                ->where(['Kode_Futsal' => $id])
        ;
        $detailDataProvider = new ActiveDataProvider([
            'query' => $detailQuery,
        ]);
        
        return $this->render('view', [
            'model' => $this->findModel($id),
            'detailDataProvider'=>$detailDataProvider
        ]);
    }

    /**
     * Creates a new Futsal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   /* public function actionCreate()
    {
        $model = new Futsal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Kode_Futsal]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/
    
     public function actionCreate() {
        $model = new Futsal();
        $details = [ new Pesertafutsal];

        // proses isi post variable 
        if ($model->load(Yii::$app->request->post())) {
            $details = Model::createMultiple(Pesertafutsal::classname());
            Model::loadMultiple($details, Yii::$app->request->post());

            // assign default transaction_id
            foreach ($details as $detail) {
                $detail->Kode_Futsal = 0;
            }

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                                ActiveForm::validateMultiple($details), ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid1 = $model->validate();
            $valid2 = Model::validateMultiple($details);
            $valid = $valid1 && $valid2;

            // jika valid, mulai proses penyimpanan
            if ($valid) {
                // mulai database transaction
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    // simpan master record                   
                    if ($flag = $model->save(false)) {
                        // simpan details record
                        foreach ($details as $detail) {
                            $detail->Kode_Futsal = $model->Kode_Futsal;
                            if (!($flag = $detail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        // sukses, commit database transaction
                        // kemudian tampilkan hasilnya
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->Kode_Futsal]);
                    } else {
                        return $this->render('create', [
                                    'model' => $model,
                                    'details' => $details,
                        ]);
                    }
                } catch (Exception $e) {
                    // penyimpanan galga, rollback database transaction
                    $transaction->rollBack();
                    throw $e;
                }
            } else {
                return $this->render('create', [
                            'model' => $model,
                            'details' => $details,
                            'error' => 'valid1: ' . print_r($valid1, true) . ' - valid2: ' . print_r($valid2, true),
                ]);
            }
        } else {
            // inisialisai id 
            // diperlukan untuk form master-detail
            $model->Kode_Futsal = 0;
            // render view
            return $this->render('create', [
                        'model' => $model,
                        'details' => $details,
            ]);
        }
    }
    
     public function actionRegister() {
        $model = new Futsal();
        $details = [ new Pesertafutsal];

        // proses isi post variable 
        if ($model->load(Yii::$app->request->post())) {
            $details = Model::createMultiple(Pesertafutsal::classname());
            Model::loadMultiple($details, Yii::$app->request->post());

            // assign default transaction_id
            foreach ($details as $detail) {
                $detail->Kode_Futsal = 0;
            }

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                                ActiveForm::validateMultiple($details), ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid1 = $model->validate();
            $valid2 = Model::validateMultiple($details);
            $valid = $valid1 && $valid2;

            // jika valid, mulai proses penyimpanan
            if ($valid) {
                // mulai database transaction
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    // simpan master record                   
                    if ($flag = $model->save(false)) {
                        // simpan details record
                        foreach ($details as $detail) {
                            $detail->Kode_Futsal = $model->Kode_Futsal;
                            if (!($flag = $detail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        // sukses, commit database transaction
                        // kemudian tampilkan hasilnya
                        $transaction->commit();
                         $this->sendmail($model->Email_Perwakilan,$model->Nama_Team);
                          Yii::$app->session->setFlash('success', Yii::$app->params['pendaftaransukses']);
                        return $this->redirect(['/site/index']);
                    } else {
                        return $this->render('create', [
                                    'model' => $model,
                                    'details' => $details,
                        ]);
                    }
                } catch (Exception $e) {
                    // penyimpanan galga, rollback database transaction
                    $transaction->rollBack();
                    throw $e;
                }
            } else {
                
                var_dump($model->getErrors());
              //  var_dump($details->getErrors());
                return $this->render('create', [
                            'model' => $model,
                            'details' => $details,
                            'error' => 'valid1: ' . print_r($valid1, true) . ' - valid2: ' . print_r($valid2, true),
                ]);
            }
        } else {
            // inisialisai id 
            // diperlukan untuk form master-detail
            $model->Kode_Futsal = 0;
            // render view
            return $this->render('create', [
                        'model' => $model,
                        'details' => $details,
            ]);
        }
    }


    /**
     * Updates an existing Futsal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
  /*  public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Kode_Futsal]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $details = $model->transactionDetails;
        if ($model->load(Yii::$app->request->post())) {
            $oldIDs = ArrayHelper::map($details, 'Kode_Peserta', 'Kode_Peserta');
            $details = Model::createMultiple(Pesertafutsal::classname(), $details);
            Model::loadMultiple($details, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($details, 'Kode_Peserta', 'Kode_Peserta')));
            // assign default transaction_id
            foreach ($details as $detail) {
                $detail->Kode_Futsal = $model->Kode_Futsal;
            }
            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($details),
                    ActiveForm::validate($model)
                );
            }
            // validate all models
            $valid1 = $model->validate();
            $valid2 = Model::validateMultiple($details);
            $valid = $valid1 && $valid2;
            // jika valid, mulai proses penyimpanan
            if ($valid) {
                // mulai database transaction
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    // simpan master record                   
                    if ($flag = $model->save(false)) {
                        // delete dahulu semua record yang ada
                        if (! empty($deletedIDs)) {
                            Pesertafutsal::deleteAll(['Kode_Peserta' => $deletedIDs]);
                        }
                        // simpan details record
                        foreach ($details as $detail) {
                            $detail->Kode_Futsal = $model->Kode_Futsal;
                            if (! ($flag = $detail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        // sukses, commit database transaction
                        // kemudian tampilkan hasilnya
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->Kode_Futsal]);
                    }
                } catch (Exception $e) {
                    // penyimpanan galga, rollback database transaction
                    $transaction->rollBack();
                    throw $e;
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'details' => $details,
                    'error' => 'valid1: '.print_r($valid1,true).' - valid2: '.print_r($valid2,true),
                ]);
            }
        }
        // render view
        return $this->render('update', [
            'model' => $model,
            'details' => (empty($details)) ? [new Pesertafutsal] : $details
        ]);
    }

    /**
     * Deletes an existing Futsal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
   /* public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/
    
       public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $details = $model->transactionDetails;
        // mulai database transaction
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // pertama, delete semua detail records
            foreach ($details as $detail) {
                $detail->delete();
            }
            // kemudian, delete master record
            $model->delete();
            // sukses, commit transaction
            $transaction->commit();
        } catch (Exception $e) {
            // gagal, rollback database transaction
            $transaction->rollBack();
        }
        return $this->redirect(['index']);
    }


    /**
     * Finds the Futsal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Futsal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Futsal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
      /**
     * Finds the TransactionDetails model based on its foreign key value.
     * @param integer $id
     * @return data provider TransactionDetails for GridView 
     */
    protected function findDetails($id)
    {
        $detailModel = new PesertafutsalSearch();
        return $detailModel->search(['PesertafutsalSearch'=>['Kode_Futsal'=>$id]]);
    }
    
     //Export Excel
    public function actionExportExcel() {
        $searchModel = new FutsalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 0;

        $style = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $styleArrayRightThin = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftThin = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftMedium = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayRightMedium = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayBottomMedium = array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayTopMedium = array(
            'borders' => array(
                'top' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $template = Yii::getAlias('@hscstudio/export') . '/templates/phpexcel/futsal.xlsx';
        $objPHPExcel = $objReader->load($template);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $baseRow = 3; // line 2
        $no = 1;
        foreach ($dataProvider->getModels() as $transaksi) {
            // Year
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $baseRow, $no);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $baseRow, $no)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
            // Month
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $baseRow, $transaksi->Nama_Team);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $baseRow, $transaksi->Nama_Team)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            // Date
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $baseRow, $transaksi->Email_Perwakilan);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $baseRow, $transaksi->Email_Perwakilan)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
  

            // Type
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $baseRow, $transaksi->No_HP);
            $objPHPExcel->getActiveSheet()->getStyle('D' . $baseRow, $transaksi->No_HP)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            
                // Category
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $baseRow, $transaksi->Keterangan);
            $objPHPExcel->getActiveSheet()->getStyle('E' . $baseRow, $transaksi->Keterangan)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

           
            $baseRow++;
            $no++;
        }
   

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="team_futsal.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }
    
         //Export Excel
    public function actionExportForm($id) {
        
        $header = $this->findModel($id);
        $detail = $this->findDetails($id);
     
        $style = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $styleArrayRightThin = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftThin = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $styleArrayLeftMedium = array(
            'borders' => array(
                'left' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayRightMedium = array(
            'borders' => array(
                'right' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayBottomMedium = array(
            'borders' => array(
                'bottom' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

        $styleArrayTopMedium = array(
            'borders' => array(
                'top' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );
        
          $styleArrayBottomThin = array(
          'borders' => array(
            'bottom' => array(
              'style' => \PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $template = Yii::getAlias('@hscstudio/export') . '/templates/phpexcel/futsalform.xlsx';
        $objPHPExcel = $objReader->load($template);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        
          $objPHPExcel->getActiveSheet()->setCellValue('B34', $header->Nama_Manager_Team);
            $objPHPExcel->getActiveSheet()->getRowDimension($baseRow)->setRowHeight(-1);
            
           $objPHPExcel->getActiveSheet()->setCellValue('B35', $header->No_HP);
             $objPHPExcel->getActiveSheet()->getRowDimension($baseRow)->setRowHeight(-1);
             
        
        
        
        $baseRowAnggota = 5; // line 2
          $baseRowNoHp = 15; // line 2
            $baseRowNik = 25; // line 2
        $no = 1;
        foreach ($detail->getModels() as $transaksi) {
            // Year
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $baseRowAnggota,  $transaksi->Nama);
        
            // Month
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $baseRowNoHp, $transaksi->No_HP);
            // Date
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $baseRowNik, $transaksi->NIK);

  

          
           
            $baseRowAnggota++;
            $baseRowNoHp++;
            $baseRowNik++;
            $no++;
        }
   

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="futsalform.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }
    
       public function sendmail($emailpenerima,$namapenerima) {
        $subject = "Konfirmasi pendaftaran Fun Futsalista #49TahunIndosatOoredoo";
         $sender = Yii::$app->params['senderMail'];
        $body = "Yth Bapak/Ibu <b>Tim " . $namapenerima . "</b><br><br>";
        $body .= "Pendaftaran anda pada event Fun Futsalista #49TahunIndosatOoredoo sudah berhasil.<br><br>";
        $body .= "Event tersebut akan dilaksanakan pada:<br>";
        $body .= "<p>Waktu : <b>17 dan 18 November 2016 | 08.00 sampai 10.00</b><br>";
        $body .= "Tempat: <b>Selasar KPPTI</b><br>";
         $body .= "Pendaftaran: <b> Ditutup 31 Oktober 2016</b><br>";
        $body .= " Peserta: <b>Karyawati Indosat Ooredoo</b><br><br>";
        
            $body .= "<b>Informasi Tambahan:</b><ul>";
        $body .= "<li>Tiap Direktorat di Indosat Ooredoo mengirimkan tim perwakilannya masing-masing (hanya 1 tim per direktorat)</li>";
        $body .= "<li>Setiap tim terdiri dari 4 orang perempuan</li>";

        $body .= "<li> Babak penyisihan 17/11/16 semua tim akan bertanding untuk menentukan 4 finalis</li>";
        $body .= "<li> Durasi pertandingan: 15 menit</li>";
        $body .= "<li> Peserta ditantang untuk menggunakan seragam bertema sebagai salah satu kriteria penilaian juri Juri & wasit</li> </ul> </p>";
         $body .= "<br><br>Terimakasih<br> -- <br>Best Regards";

        $headers = "From: <{$sender}>\r\n" .
                "Reply-To: {$sender}\r\n" .
                "MIME-Version: 1.0\r\n" .
                "Content-Transfer-Encoding: 8bit\r\n" .
                "Content-Type: text/html; charset=ISO-8859-1\r\n";

        mail($emailpenerima, $subject, $body, $headers); 
      
    }
}
