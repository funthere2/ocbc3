<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "voting".
 *
 * @property integer $Kode_Voting
 * @property string $Nama
 * @property integer $Pilihan
 * @property string $Email
 * @property string $field1
 * @property string $field2
 * @property integer $created
 * @property integer $updated
 */
class Voting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'Pilihan','field1','field2'], 'required'],
             [['Email'], 'email'],
            //['Email', 'unique', 'message' => 'Anda sudah melakukan voting'],
            [['Pilihan', 'created', 'updated','field1','field2'], 'integer'],
            [['Nama', 'Email', 'field1', 'field2'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kode_Voting' => 'Kode  Voting',
            'Nama' => 'Nama Lengkap',
            'Pilihan' => 'No  Foto favorit 1',
            'Email' => 'Email',
            'field1' => 'No  Foto favorit 2',
            'field2' => 'No  Foto favorit 3',
            'created' => 'Tanggal Voting',
            'updated' => 'Updated',
        ];
    }
    
      public function beforeSave($insert) {
       if (parent::beforeSave($insert)) {          
           if ($this->isNewRecord) {
           $this->created = time();
           $this->updated = time();
           return true;
            } else {   
                $this->updated = time();
                return true;
            }
       }
       return false;
   }
}
