<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Citybike;

/**
 * CitybikeSearch represents the model behind the search form about `app\models\Citybike`.
 */
class CitybikeSearch extends Citybike
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode_Citybike'], 'integer'],
            [['Nama_Peserta', 'Email_Peserta', 'No_HP', 'NIK', 'Kategori_City_Bike', 'Ukuran_Jersey', 'Nama_Anggota_Keluarga', 'Keterangan','Jenis_Kelamin','Usia'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Citybike::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Kode_Citybike' => $this->Kode_Citybike,
        ]);

        $query->andFilterWhere(['like', 'Nama_Peserta', $this->Nama_Peserta])
            ->andFilterWhere(['like', 'Email_Peserta', $this->Email_Peserta])
            ->andFilterWhere(['like', 'No_HP', $this->No_HP])
            ->andFilterWhere(['like', 'NIK', $this->NIK])
            ->andFilterWhere(['like', 'Kategori_City_Bike', $this->Kategori_City_Bike])
            ->andFilterWhere(['like', 'Ukuran_Jersey', $this->Ukuran_Jersey])
            ->andFilterWhere(['like', 'Nama_Anggota_Keluarga', $this->Nama_Anggota_Keluarga])
            ->andFilterWhere(['like', 'Jenis_Kelamin', $this->Jenis_Kelamin])
              ->andFilterWhere(['like', 'Usia', $this->Usia])
            ->andFilterWhere(['like', 'Keterangan', $this->Keterangan]);

        return $dataProvider;
    }
}
