<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "citybike".
 *
 * @property integer $Kode_Citybike
 * @property string $Nama_Peserta
 * @property string $Email_Peserta
 * @property string $No_HP
 * @property string $NIK
 * @property string $Kategori_City_Bike
 * @property string $Ukuran_Jersey
 * @property string $Nama_Anggota_Keluarga
 * @property string $Keterangan
 */
class Citybike extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'citybike';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nama_Peserta', 'Email_Peserta', 'No_HP', 'NIK', 'Kategori_City_Bike', 'Ukuran_Jersey','Jenis_Kelamin','Nama_Anggota_Keluarga','Usia','Keterangan'], 'required'],
            [['Keterangan'], 'string'],
              [['Email_Peserta'], 'email'],
            [['Nama_Peserta'], 'string', 'max' => 250],
            [['Email_Peserta', 'No_HP', 'NIK', 'Kategori_City_Bike', 'Ukuran_Jersey', 'Nama_Anggota_Keluarga','Usia'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kode_Citybike' => 'Kode  Citybike',
            'Nama_Peserta' => 'Nama  Peserta',
            'Email_Peserta' => 'Email  Peserta',
            'No_HP' => 'No  Hp',
            'NIK' => 'NIK',
            'Kategori_City_Bike' => 'Jarak Tempuh',
            'Ukuran_Jersey' => 'Ukuran  Jersey',
            'Nama_Anggota_Keluarga' => 'Departemen',
            'Keterangan' => 'Jenis Sepeda Yang Digunakan',
            'Jenis_Kelamin'=>'Jenis Kelamin',
            'Usia'=>'Usia'
        ];
    }
    
      /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionDetails()
    {
        return $this->hasMany(Anggotakeluarga::className(), ['Kode_Citybike' => 'Kode_Citybike']);
    }
}
