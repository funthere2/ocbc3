<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pesertacharitywalk".
 *
 * @property integer $Kode_Peserta
 * @property integer $Kode_Charitywalk
 * @property string $Nama
 * @property string $NIK
 * @property string $Jenis_Kelamin
 * @property string $No_HP
 * @property string $Keterangan
 */
class Pesertacharitywalk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pesertacharitywalk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode_Charitywalk', 'Nama', 'NIK', 'Jenis_Kelamin','Keterangan'], 'required'],
            [['Kode_Charitywalk'], 'integer'],
            [['Keterangan'], 'email'],
            [['Nama', 'NIK', 'Jenis_Kelamin', 'No_HP'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kode_Peserta' => 'Kode  Peserta',
            'Kode_Charitywalk' => 'Team',
            'Nama' => 'Nama',
            'NIK' => 'NIK',
            'Jenis_Kelamin' => 'Jenis  Kelamin',
            'No_HP' => 'No  Hp',
            'Keterangan' => 'Email',
        ];
    }
    
    public function getCharitywalk()
    {
        return $this->hasOne(Charitywalk::className(), ['Kode_Charitywalk' => 'Kode_Charitywalk']);
    }
}
