<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pesertacharitywalk;

/**
 * PesertacharitywalkSearch represents the model behind the search form about `app\models\Pesertacharitywalk`.
 */
class PesertacharitywalkSearch extends Pesertacharitywalk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode_Peserta', 'Kode_Charitywalk'], 'integer'],
            [['Nama', 'NIK', 'Jenis_Kelamin', 'No_HP', 'Keterangan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pesertacharitywalk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Kode_Peserta' => $this->Kode_Peserta,
            'Kode_Charitywalk' => $this->Kode_Charitywalk,
        ]);

        $query->andFilterWhere(['like', 'Nama', $this->Nama])
            ->andFilterWhere(['like', 'NIK', $this->NIK])
            ->andFilterWhere(['like', 'Jenis_Kelamin', $this->Jenis_Kelamin])
            ->andFilterWhere(['like', 'No_HP', $this->No_HP])
            ->andFilterWhere(['like', 'Keterangan', $this->Keterangan]);

        return $dataProvider;
    }
}
