<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Anggotakeluarga;

/**
 * AnggotakeluargaSearch represents the model behind the search form about `app\models\Anggotakeluarga`.
 */
class AnggotakeluargaSearch extends Anggotakeluarga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode_Peserta', 'Kode_Citybike'], 'integer'],
            [['Nama', 'Jenis_Kelamin', 'Ukuran_Jersey', 'Usia', 'Jenis_Sepeda', 'field1', 'filed2', 'field3'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Anggotakeluarga::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Kode_Peserta' => $this->Kode_Peserta,
            'Kode_Citybike' => $this->Kode_Citybike,
        ]);

        $query->andFilterWhere(['like', 'Nama', $this->Nama])
            ->andFilterWhere(['like', 'Jenis_Kelamin', $this->Jenis_Kelamin])
            ->andFilterWhere(['like', 'Ukuran_Jersey', $this->Ukuran_Jersey])
            ->andFilterWhere(['like', 'Usia', $this->Usia])
            ->andFilterWhere(['like', 'Jenis_Sepeda', $this->Jenis_Sepeda])
            ->andFilterWhere(['like', 'field1', $this->field1])
            ->andFilterWhere(['like', 'filed2', $this->filed2])
            ->andFilterWhere(['like', 'field3', $this->field3]);

        return $dataProvider;
    }
}
