<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "futsal".
 *
 * @property integer $Kode_Futsal
 * @property string $Nama_Team
 * @property string $Nama_Manager_Team
 * @property string $Email_Perwakilan
 * @property string $No_HP
 * @property string $Keterangan
 */
class Futsal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'futsal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nama_Team', 'Nama_Manager_Team', 'Email_Perwakilan', 'No_HP'], 'required'],
            [['Keterangan'], 'string'],
            [['Nama_Team', 'Nama_Manager_Team', 'No_HP'], 'string', 'max' => 250],
            [['Email_Perwakilan'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kode_Futsal' => 'Kode  Futsal',
            'Nama_Team' => 'Nama  Team',
            'Nama_Manager_Team' => 'Nama  Manager  Team',
            'Email_Perwakilan' => 'Email  Perwakilan',
            'No_HP' => 'No  Hp',
            'Keterangan' => 'Keterangan',
        ];
    }
    
      /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionDetails()
    {
        return $this->hasMany(Pesertafutsal::className(), ['Kode_Futsal' => 'Kode_Futsal']);
    }
}
