<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pesertafutsal".
 *
 * @property integer $Kode_Peserta
 * @property integer $Kode_Futsal
 * @property string $Nama
 * @property string $NIK
 * @property string $Jenis_Kelamin
 * @property string $No_HP
 * @property string $Keterangan
 */
class Pesertafutsal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pesertafutsal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode_Futsal', 'Nama', 'NIK', 'Jenis_Kelamin','Keterangan'], 'required'],
            [['Kode_Futsal'], 'integer'],
            [['Keterangan'], 'email'],
            [['Nama', 'NIK'], 'string', 'max' => 250],
            [['Jenis_Kelamin'], 'string', 'max' => 50],
            [['No_HP'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kode_Peserta' => 'Kode  Peserta',
            'Kode_Futsal' => 'Kode  Futsal',
            'Nama' => 'Nama',
            'NIK' => 'NIK',
            'Jenis_Kelamin' => 'Jenis  Kelamin',
            'No_HP' => 'No  Hp',
            'Keterangan' => 'Email',
        ];
    }
    
     public function getFutsal()
    {
        return $this->hasOne(Futsal::className(), ['Kode_Futsal' => 'Kode_Futsal']);
    }
}
