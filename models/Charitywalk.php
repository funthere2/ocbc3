<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charitywalk".
 *
 * @property integer $Kode_Charitywalk
 * @property string $Nama_Team
 * @property string $Email_Perwakilan
 * @property string $No_HP
 * @property string $Keterangan
 */
class Charitywalk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'charitywalk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nama_Team', 'Email_Perwakilan', 'No_HP'], 'required'],
            [['Keterangan'], 'string'],
            [['Nama_Team', 'No_HP'], 'string', 'max' => 250],
            [['Email_Perwakilan'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kode_Charitywalk' => 'Kode  Charitywalk',
            'Nama_Team' => 'Nama  Team',
            'Email_Perwakilan' => 'Email  Perwakilan',
            'No_HP' => 'No  Hp',
            'Keterangan' => 'Keterangan',
        ];
    }
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionDetails()
    {
        return $this->hasMany(Pesertacharitywalk::className(), ['Kode_Charitywalk' => 'Kode_Charitywalk']);
    }
}
