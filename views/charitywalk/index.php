<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CharitywalkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Charity Walks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charitywalk-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Register Charity Walk', ['create'], ['class' => 'btn btn-success']) ?>
         <?= Html::a('Semua Peserta', ['/pesertacharitywalk/index'], ['class' => 'btn btn-primary']) ?>
        
            <?php
        $urlexcel = array_filter(Yii::$app->getRequest()->get());
        $urlexcel[0] = 'export-excel';
        unset($urlexcel['page']);
        ?>

        <?= Html::a('Export Excel ', $urlexcel, ['class' => 'btn btn-info', 'target' => '_blank']); ?>
        
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=> Yii::$app->params['summary'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Kode_Charitywalk',
            'Nama_Team',
            'Email_Perwakilan:email',
            'No_HP',
            'Keterangan:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
