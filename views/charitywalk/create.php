<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Charitywalk */

$this->title = 'Register Digital Charity Run';
//$this->params['breadcrumbs'][] = ['label' => 'Charitywalks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charitywalk-create">

  

    <?= $this->render('_form', [
        'model' => $model,
        'details' => $details,
    ]) ?>

</div>
