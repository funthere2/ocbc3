<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Charitywalk */

$this->title = 'Update Charity Walk: ' . $model->Kode_Charitywalk;
//$this->params['breadcrumbs'][] = ['label' => 'Charitywalks', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->Kode_Charitywalk, 'url' => ['view', 'id' => $model->Kode_Charitywalk]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charitywalk-update">

  

    <?= $this->render('_form', [
        'model' => $model,
         'details' => $details,
    ]) ?>

</div>
