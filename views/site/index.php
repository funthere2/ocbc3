<?php
/* @var $this yii\web\View */

$this->title = Yii::$app->name;

use yii\helpers\Html;
$this->title = '#49TahunIndosatOoredoo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="jumbotron">
        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i>Terimakasih...</h4>
                <?= Yii::$app->session->getFlash('success') ?>
            </div>
        <?php endif; ?>


        <p class="lead">Ayo meriahkan perayaan menuju <b>#49TahunIndosatOoredoo</b>  <br> dengan mengikuti kegiatan dibawah ini.<br>Daftarkan dirimu sekarang juga!

        </p>

        <center> <?php echo '<img src="' . Yii::getAlias('@web') . '/images/indosat.png" width="300">'; ?></center>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Digital Charity Run</h2>
                <div class="img-responsive">
                    <?php echo '<img src="' . Yii::getAlias('@web') . '/images/lari.jpg" width="300">'; ?>
                </div>
                <br>
                <p>Waktu: <b>28 Oktober 2016 | 07.00 - 09.00 WIB</b><br>
                    Tempat: <b>Start - finish KPPTI</b><br>
                    Pendaftaran: <b> Hingga 21 Oktober 2016</b><br>

                    Peserta: <b>Karyawan Indosat Ooredoo</b><br><br>
                    <b> Syarat dan Ketentuan:</b>
                <ul> 
                    <li>Tiap Direktorat di Indosat Ooredoo mengirimkan tim perwakilannya masing-masing</li>
                    <li>1 tim terdiri dari 10 orang anggota. Tiap direktorat boleh mengirim lebih dari 1 tim</li>
                 
                    <li> Para peserta akan berlari selama 49 menit dengan start dan finish di KPPTI. </li>
                    <li>Setiap langkah peserta akan dikonfersi dalam rupiah untuk disumbangkan pada program CSR Indosat Ooredoo</li>
                     <li> Tim dengan kostum terunik akan mendapat hadiah menarik</li>
                </ul> 
                </p>
                <p>

                    <?= Html::a('Register', ['/charitywalk/register'], ['class' => 'btn btn-lg btn-success']); ?>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Fun Futsalista</h2>
                 <div class="img-responsive">
                <?php echo '<img src="' . Yii::getAlias('@web') . '/images/futsal.jpg" width="300">'; ?>
                 </div>
               <br> <p>Waktu : <b>17 dan 18 November 2016 | 08.00 - 10.00</b><br>
                    Tempat: <b>Selasar KPPTI</b><br>
                    Pendaftaran: <b> Ditutup 31 Oktober 2016</b><br>

                    Peserta: <b>Karyawati Indosat Ooredoo</b><br><br>

                    <b>Syarat dan Ketentuan:</b><br>
                <ul>
                    <li>Tiap Direktorat di Indosat Ooredoo mengirimkan tim perwakilannya masing-masing (hanya 1 tim per direktorat)</li>
                    <li>Setiap tim terdiri dari 4 orang perempuan</li>
                    <li>Babak penyisihan 17/11/16 semua tim akan bertanding untuk menentukan 4 finalis</li>
                    <li>  Durasi pertandingan: 15 menit</li>
                    <li>  Peserta ditantang untuk menggunakan seragam bertema sebagai salah satu kriteria penilaian juri Juri & wasit</li>
                </ul>
                </p>

                <p>
                    <?= Html::a('Register', ['/futsal/register'], ['class' => 'btn btn-lg btn-danger']); ?>
                </p>

            </div>
            <div class="col-lg-4">
                <h2>Gowes Bareng </h2>
                 <div class="img-responsive">
                <?php echo '<img src="' . Yii::getAlias('@web') . '/images/funbike.jpg" width="300">'; ?>
                 </div>
                <br><p>Waktu: <b>13 November 2016 | 06.00 - 13.00</b><br>
                    Tempat: <b> Start - Finish di KPPTI</b><br>
                    Jarak : <b>49 KM & 4,9 KM </b>(Jalur akan diberitahukan kemudian <br>
                    Pendaftaran: <b>Ditutup 31 Okt 2016, pukul 11.00 WIB</b><br><br>

                    <b> Syarat dan Ketentuan:</b><br><ul>
                    <li> Berlaku untuk Karyawan Tetap, OS, PKWT beserta keluarga</li>
                    <li>  Setiap peserta wajib membayar biaya Pendaftaran: Rp 49.000/peserta (mendapatkan Race pack termasuk Jersey)</li>
                    <li>  Biaya pendaftaran dapat dikirimkan ke akun Dompetku atas nama <b>Irene Angelina Sugianto</b> dengan no hp <b>08551323623</b> paling lambat tanggal 31 Oktober 2016. Bukti pembayaran kirim ke <b>irene.sugianto@indosatooredoo.com</b>.</li>
                    <li>  Peserta membawa sepedanya masing-masing untuk mengikuti fun bike. Panitia tidak menyediakan peminjaman/ penyewaan</li>
                    <li>  Pembagian Race Pack: 11 November 2016</li>
                    <li>  Untuk informasi lebih lanjut hubungi <b>Achmadi Prasetyo</b>: <b>achmadi.prasetyo@indosatooredoo.com </b>/ 08557890111</li>
                
                </ul>
                </p>

                <p>

                    <?= Html::a('Register', ['/citybike/registerawal'], ['class' => 'btn btn-lg btn-warning']); ?>
                </p>
            </div>
        </div>

    </div>

</div>
	