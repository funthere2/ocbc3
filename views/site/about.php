<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = ' “It is never too late to learn.” – Malcolm Forbes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
       
Di luar negeri sepertinya banyak orang yang bekerja di dunia keuangan dan investasi yang membaca majalah keuangan seperti Forbes. Malcolm Forbes sering mengingatkan di kata-katanya, bahwa setiap orang bisa menjadi sukses dalam keuangan mereka tanpa memandang usia, latar belakang, dan status sosial. 

Belajar adalah proses yang berkesinambungan dan terus menerus seumur hidup kita. Dan dari belajar itulah kita bisa mendapatkan sesuatu untuk meningkatkan taraf hidup dan keuangan kita. Tidak pernah ada kata terlambat.
    </p>

   
</div>
