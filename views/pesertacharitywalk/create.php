<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pesertacharitywalk */

$this->title = 'Create Pesertacharitywalk';
$this->params['breadcrumbs'][] = ['label' => 'Pesertacharitywalks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pesertacharitywalk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
