<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PesertacharitywalkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Peserta Charity Walks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pesertacharitywalk-index">

   
<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('Charity Walk', ['/charitywalk/index'], ['class' => 'btn btn-success']) ?>

        <?php
        $urlexcel = array_filter(Yii::$app->getRequest()->get());
        $urlexcel[0] = 'export-excel';
        unset($urlexcel['page']);
        ?>

        <?= Html::a('Export Excel ', $urlexcel, ['class' => 'btn btn-info', 'target' => '_blank']); ?>
        

    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=> Yii::$app->params['summary'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'Kode_Peserta',
            //'Kode_Charitywalk',
            [
                'attribute' => 'Kode_Charitywalk',
                'value' => 'charitywalk.Nama_Team',
                'filter' => Html::activeDropDownList($searchModel, 'Kode_Charitywalk', ArrayHelper::map(\app\models\Charitywalk::find()->orderBy(['Nama_Team' => SORT_ASC])->asArray()->all(), 'Kode_Charitywalk', 'Nama_Team'), ['class' => 'form-control', 'prompt' => 'Semua Team']),
            ],
            'Nama',
            'NIK',
            'Jenis_Kelamin',
            'No_HP',
        // 'Keterangan:ntext',
        //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
