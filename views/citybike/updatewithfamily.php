<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Citybike */

$this->title = 'Update Citybike: ' . $model->Kode_Citybike;
//$this->params['breadcrumbs'][] = ['label' => 'Citybikes', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->Kode_Citybike, 'url' => ['view', 'id' => $model->Kode_Citybike]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="citybike-update">



    <?= $this->render('_formwithfamily', [
        'model' => $model,
        'details' => $details,
    ]) ?>

</div>
