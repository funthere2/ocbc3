<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use wbraganca\dynamicform\DynamicFormWidget;


/* @var $this yii\web\View */
/* @var $model app\models\Charitywalk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transactions-form">

    <?php $form = ActiveForm::begin(['id' => 'transactions-form']); ?>

 <?= $form->field($model, 'Nama_Peserta')->textInput(['maxlength' => true]) ?>
    
      <?= $form->field($model, 'Jenis_Kelamin')->dropDownList(Yii::$app->params['jeniskelamin']);?>
        
    <?= $form->field($model, 'Usia')->textInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'NIK')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'Nama_Anggota_Keluarga')->textInput(['maxlength' => true]) ?>
    
    
    <?= $form->field($model, 'Email_Peserta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'No_HP')->textInput(['maxlength' => true]) ?>   

      <?=$form->field($model, 'Kategori_City_Bike')->dropDownList(Yii::$app->params['kategoricitybike']);?>

     <?=$form->field($model, 'Ukuran_Jersey')->dropDownList(Yii::$app->params['ukuranjersey']);?>

    
    
     <?=$form->field($model, 'Keterangan')->dropDownList(Yii::$app->params['jenissepeda']);?>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-th-list"></i> Anggota keluarga yang ikut</h4></div>
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper',  // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items',          // required: css class selector
                'widgetItem' => '.item',                     // required: css class
                'limit' => 20,                                // the maximum times, an element can be cloned (default 999)
                'min' => 1,                                  // 0 or 1 (default 1)
                'insertButton' => '.add-item',               // css class
                'deleteButton' => '.remove-item',            // css class
                'model' => $details[0],
                'formId' => 'transactions-form',
                'formFields' => [
                    'trans_id',
                    'item_id',
                    'quantity',
                    'remarks',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($details as $i => $detail): ?>
                <div class="item row">
                    <?php
                        // necessary for update action.
                        if (! $detail->isNewRecord) {
                            echo Html::activeHiddenInput($detail, "[{$i}]Kode_Citybike");
                        }
                    ?>
                    <div class="col-sm-6 col-md-2">
                         <?= $form->field($detail, "[{$i}]Nama")->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6 col-md-1">
                        <?= $form->field($detail, "[{$i}]Usia")->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6 col-md-2">
                    	  <?=$form->field($detail, "[{$i}]Jenis_Kelamin")->dropDownList(Yii::$app->params['jeniskelamin']);?>
     
                    </div>
                    
                     <div class="col-sm-6 col-md-2">
                    	 <?=$form->field($detail, "[{$i}]Ukuran_Jersey")->dropDownList(Yii::$app->params['ukuranjerseycitybike']);?>
                    </div>
                    
                    <div class="col-sm-6 col-md-2">
                    	 <?=$form->field($detail, "[{$i}]field1")->dropDownList(Yii::$app->params['kategoricitybike']);?>
                    </div>
                    
                       <div class="col-sm-6 col-md-2">
                    	<?= $form->field($detail, "[{$i}]Jenis_Sepeda")->dropDownList(Yii::$app->params['jenissepeda']);?>
                    </div>
                    
                    <div class="col-sm-2 col-md-1 item-action">
                    	<div class="pull-right">
	                        <button type="button" class="add-item btn btn-success btn-xs">
	                        	<i class="glyphicon glyphicon-plus"></i></button> 
	                        <button type="button" class="remove-item btn btn-danger btn-xs">
	                        	<i class="glyphicon glyphicon-minus"></i></button>
                    	</div>
                    </div>
                </div><!-- .row -->

            <?php endforeach; ?>
            </div>

            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Register') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
