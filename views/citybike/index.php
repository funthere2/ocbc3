<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CitybikeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gowes Bareng';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="citybike-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Register City Bike', ['registerawal'], ['class' => 'btn btn-success']) ?>
        
          <?= Html::a('Anggota Keluarga Yg Ikut', ['/anggotakeluarga/index'], ['class' => 'btn btn-primary']) ?>

        <?php
        $urlexcel = array_filter(Yii::$app->getRequest()->get());
        $urlexcel[0] = 'export-excel';
        unset($urlexcel['page']);
        ?>

        <?= Html::a('Export Excel ', $urlexcel, ['class' => 'btn btn-info', 'target' => '_blank']); ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => Yii::$app->params['summary'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'Kode_Citybike',
            'Nama_Peserta',
            'Jenis_Kelamin',
            'Email_Peserta:email',
            'No_HP',
            'NIK',
            'Kategori_City_Bike',
            'Ukuran_Jersey',
            'Nama_Anggota_Keluarga',
            'Keterangan',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{delete}'
            ],
        ],
    ]);
    ?>
</div>
