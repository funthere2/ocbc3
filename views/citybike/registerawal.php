<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CitybikeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Apakah ada anggota keluarga yang ikut serta pada gowes bareng?';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="citybike-index">

 
    <p>
        <?= Html::a('Ada', ['/citybike/registerwithfamily'], ['class' => 'btn btn-success']) ?>
        
       <?= Html::a('Tidak Ada', ['/citybike/register'], ['class' => 'btn btn-primary']) ?>
    </p>
  
</div>
