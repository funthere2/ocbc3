<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CitybikeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="citybike-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Kode_Citybike') ?>

    <?= $form->field($model, 'Nama_Peserta') ?>

    <?= $form->field($model, 'Email_Peserta') ?>

    <?= $form->field($model, 'No_HP') ?>

    <?= $form->field($model, 'NIK') ?>

    <?php // echo $form->field($model, 'Kategori_City_Bike') ?>

    <?php // echo $form->field($model, 'Ukuran_Jersey') ?>

    <?php // echo $form->field($model, 'Nama_Anggota_Keluarga') ?>

    <?php // echo $form->field($model, 'Keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
