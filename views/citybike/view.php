<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Citybike */

$this->title = "Peserta : ".$model->Nama_Peserta;
//$this->params['breadcrumbs'][] = ['label' => 'Citybikes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="citybike-view">



    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kode_Citybike], ['class' => 'btn btn-primary']) ?>
         <?= Html::a('Update With Family', ['updatewithfamily', 'id' => $model->Kode_Citybike], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kode_Citybike], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a('Export Form ', ['export-form','id'=>$model->Kode_Citybike], ['class' => 'btn btn-info', 'target' => '_blank']); ?>
        <?= Html::a('List', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kode_Citybike',
            'Nama_Peserta',
            'Jenis_Kelamin',
            'Email_Peserta:email',
            'No_HP',
            'NIK',
            'Kategori_City_Bike',
            'Ukuran_Jersey',
            'Nama_Anggota_Keluarga',
            'Keterangan',
        ],
    ]) ?>
    
      <h3>Daftar anggota Keluarga yg ikut</h3>
    <?=
    GridView::widget([
        'dataProvider' => $detailDataProvider,
        //  'filterModel' => $searchModel,
        'summary' => Yii::$app->params['summary'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           // 'Kode_Peserta',
            //'Kode_Charitywalk',
            'Nama',
            'Usia',
            'Jenis_Kelamin',
             'Ukuran_Jersey',
            'field1',
          
             'Jenis_Sepeda',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
