<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PesertafutsalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Peserta Futsal';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pesertafutsal-index">

   
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Fun Futsal', ['/charitywalk/index'], ['class' => 'btn btn-success']) ?>

        <?php
        $urlexcel = array_filter(Yii::$app->getRequest()->get());
        $urlexcel[0] = 'export-excel';
        unset($urlexcel['page']);
        ?>

        <?= Html::a('Export Excel ', $urlexcel, ['class' => 'btn btn-info', 'target' => '_blank']); ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=> Yii::$app->params['summary'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'Kode_Peserta',
            //'Kode_Futsal',
             [
                'attribute' => 'Kode_Futsal',
                'value' => 'futsal.Nama_Team',
                'filter' => Html::activeDropDownList($searchModel, 'Kode_Futsal', ArrayHelper::map(\app\models\Futsal::find()->orderBy(['Nama_Team' => SORT_ASC])->asArray()->all(), 'Kode_Futsal', 'Nama_Team'), ['class' => 'form-control', 'prompt' => 'Semua Team']),
            ],
            'Nama',
            'NIK',
            'Jenis_Kelamin',
             'No_HP',
             'Keterangan:email',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
