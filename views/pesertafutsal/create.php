<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pesertafutsal */

$this->title = 'Create Pesertafutsal';
$this->params['breadcrumbs'][] = ['label' => 'Pesertafutsals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pesertafutsal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
