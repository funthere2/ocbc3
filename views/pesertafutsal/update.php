<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pesertafutsal */

$this->title = 'Update Pesertafutsal: ' . $model->Kode_Peserta;
$this->params['breadcrumbs'][] = ['label' => 'Pesertafutsals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kode_Peserta, 'url' => ['view', 'id' => $model->Kode_Peserta]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pesertafutsal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
