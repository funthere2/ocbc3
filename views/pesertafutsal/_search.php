<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PesertafutsalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pesertafutsal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Kode_Peserta') ?>

    <?= $form->field($model, 'Kode_Futsal') ?>

    <?= $form->field($model, 'Nama') ?>

    <?= $form->field($model, 'NIK') ?>

    <?= $form->field($model, 'Jenis_Kelamin') ?>

    <?php // echo $form->field($model, 'No_HP') ?>

    <?php // echo $form->field($model, 'Keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
