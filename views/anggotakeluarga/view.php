<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Anggotakeluarga */

$this->title = $model->Kode_Peserta;
//$this->params['breadcrumbs'][] = ['label' => 'Anggotakeluargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anggotakeluarga-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kode_Peserta], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kode_Peserta], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kode_Peserta',
            'Kode_Citybike',
            'Nama',
            'Jenis_Kelamin',
            'Ukuran_Jersey',
            'Usia',
            'Jenis_Sepeda',
            'field1',
            'filed2',
            'field3',
        ],
    ]) ?>

</div>
