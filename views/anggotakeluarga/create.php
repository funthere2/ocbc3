<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Anggotakeluarga */

$this->title = 'Create Anggotakeluarga';
$this->params['breadcrumbs'][] = ['label' => 'Anggotakeluargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anggotakeluarga-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
