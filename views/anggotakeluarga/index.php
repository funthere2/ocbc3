<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AnggotakeluargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Anggota Keluarga Yang Ikut';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anggotakeluarga-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
             <?= Html::a('City Bike', ['/citybike/index'], ['class' => 'btn btn-success']) ?>

        <?php
        $urlexcel = array_filter(Yii::$app->getRequest()->get());
        $urlexcel[0] = 'export-excel';
        unset($urlexcel['page']);
        ?>

        <?= Html::a('Export Excel ', $urlexcel, ['class' => 'btn btn-info', 'target' => '_blank']); ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>  Yii::$app->params['summary'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'Kode_Peserta',
            //'Kode_Citybike',
              [
                'attribute' => 'Kode_Citybike',
                'value' => 'citybike.Nama_Peserta',
                'filter' => Html::activeDropDownList($searchModel, 'Kode_Citybike', ArrayHelper::map(\app\models\Citybike::find()->orderBy(['Nama_Peserta' => SORT_ASC])->asArray()->all(), 'Kode_Citybike', 'Nama_Peserta'), ['class' => 'form-control', 'prompt' => 'Semua Peserta']),
            ],
            'Nama',
            'Jenis_Kelamin',
            'Ukuran_Jersey',
             'Usia',
             'Jenis_Sepeda',
             'field1',
            // 'filed2',
            // 'field3',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
