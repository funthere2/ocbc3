<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Anggotakeluarga */

$this->title = 'Update Anggota Keluarga: ' . $model->Kode_Peserta;
//$this->params['breadcrumbs'][] = ['label' => 'Anggotakeluargas', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->Kode_Peserta, 'url' => ['view', 'id' => $model->Kode_Peserta]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anggotakeluarga-update">

  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
