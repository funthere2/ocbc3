<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Anggotakeluarga */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="anggotakeluarga-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Kode_Citybike')->textInput() ?>

    <?= $form->field($model, 'Nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jenis_Kelamin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Ukuran_Jersey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Usia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Jenis_Sepeda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'field1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'filed2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'field3')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
