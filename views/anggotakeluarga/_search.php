<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AnggotakeluargaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="anggotakeluarga-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Kode_Peserta') ?>

    <?= $form->field($model, 'Kode_Citybike') ?>

    <?= $form->field($model, 'Nama') ?>

    <?= $form->field($model, 'Jenis_Kelamin') ?>

    <?= $form->field($model, 'Ukuran_Jersey') ?>

    <?php // echo $form->field($model, 'Usia') ?>

    <?php // echo $form->field($model, 'Jenis_Sepeda') ?>

    <?php // echo $form->field($model, 'field1') ?>

    <?php // echo $form->field($model, 'filed2') ?>

    <?php // echo $form->field($model, 'field3') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
