<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="shortcut icon" type="image/png" href="<?php echo Yii::getAlias('@web') ?>/images/indosaticon.png"/>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => Html::img('@web/images/indosaticon.png', ['alt' => Yii::$app->name]),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top my-navbar',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Home', 'url' => ['/site/index']],
                    ['label' => 'Digital Charity Run', 'url' => ['/charitywalk/register']],
                    ['label' => 'Fun Futsalista', 'url' => ['/futsal/register']],
                    ['label' => 'Gowes Bareng', 'url' => ['/citybike/registerawal']],
                    ['label' => 'Event Lainnya', 'url' => ['/site/eventlainnya']],
                    ['label' => 'Rute', 'url' => ['/site/rute'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => 'Voting', 'url' => ['/voting/index']],
                    ['label' => 'Administrator', 'url' => ['/transaksi/index'], 'items' => [

                            ['label' => 'Digital Charity Run', 'url' => ['/charitywalk/index']],
                            ['label' => 'Fun Futsalista', 'url' => ['/futsal/index']],
                            ['label' => 'Gowes Bareng', 'url' => ['/citybike/index']],
                            ['label' => 'Voting', 'url' => ['/voting/list']],
                            
                            ['label' => 'Pengguna', 'url' => ['/administrator/user']],
                            ['label' => 'Group Pengguna', 'url' => ['/administrator/role']],
                            ['label' => 'Aksi', 'url' => ['/administrator/route']],
                        ], 'visible' => !Yii::$app->user->isGuest],
                    Yii::$app->user->isGuest ? (
                            ['label' => 'Rute', 'url' => ['/site/rute']]
                            ) : (
                            '<li>'
                            . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                            . Html::submitButton(
                                    'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link']
                            )
                            . Html::endForm()
                            . '</li>'
                            )
                ],
            ]);
            NavBar::end();
            ?>

            <div class="container">
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'homeLink' => false, // add this line
            ])
            ?>
                <?= $content ?>
            </div>
                <br><br><br><br><br><br>
        </div>

        <footer class="footer">
             
             <div class="footer-bokeh"></div>
            <div class="container">
               
                
                <p class="pull-left">49 Tahun Indosat Ooredoo </p>






            </div>
        </footer>

<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
