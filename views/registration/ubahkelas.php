<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>

	<br><br>
	<?= Html::label('Silahkan pilih kelas') ?>

	<?= Html::label('Pilih Kelas') ?>
	<?= Html:: dropDownList ('text', null, ['1'=>'Kelas A','2'=>'Kelas B','3'=>'Kelas C'], array('class' => 'form-control')) ?>


	<br><br>
    <div class="form-group">
    <?= Html::a('Ubah', ['emailperubahankelas'], ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
