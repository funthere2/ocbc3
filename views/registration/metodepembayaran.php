<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= Html::label('Pilih Metode Pembayaran') ?>
	<?= Html:: dropDownList ('text', null, ['1'=>'Transfer','2'=>'Credit Card'], array('class' => 'form-control')) ?>

	<?= Html::label('Pilih Bank') ?>
	<?= Html:: dropDownList ('text', null, ['1'=>'Ocbc','2'=>'Mandiri'], array('class' => 'form-control')) ?>

	<br><br>
	<?= Html::label('Silahkan melakukan pembayaran ke bank berikut:') ?>
	
	<table>
		<tr>
			<td width="100px">Bank</td> <td width="20px">:</td> <td>Mandiri</td>
		</tr>
		<tr>
			<td>No Rekening</td> <td>:</td> <td>08098999xxx</td>
		</tr>
		<tr>
			<td>Atas Nama</td> <td>:</td> <td>Ocbc Bank</td>
		</tr>
	</table>

	<br><br>
    <div class="form-group">
    <?= Html::a('Selanjutnya', ['emailsukses'], ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
