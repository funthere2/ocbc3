<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>

	<br><br>
	<?= Html::label('Anda sedang login ke dalam sistem') ?>

	<br><br>
	<p>Untuk mengubah kelas, klik link ini.</p>
    <?= Html::a('Ubah Kelas', ['ubahkelas'], ['class' => '']) ?>

    <br><br>
	<p>Atau untuk menerima e-ticket, klik link ini.</p>
    <?= Html::a('Terima e-ticket', ['eticket'], ['class' => '']) ?>



    <?php ActiveForm::end(); ?>

</div>
