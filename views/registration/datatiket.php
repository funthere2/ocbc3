<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= Html::label('Personal ID') ?>
	<?= Html::input('text', 'null', '', array('class' => 'form-control')) ?>

	<?= Html::label('Full Name') ?>
	<?= Html::input('text', 'null', '', array('class' => 'form-control')) ?>

	<?= Html::label('Email') ?>
	<?= Html::input('text', 'null', '', array('class' => 'form-control')) ?>

	<?= Html::label('Cell Phone') ?>
	<?= Html::input('text', 'null', '', array('class' => 'form-control')) ?>

	<?= Html::label('Gender') ?>
	<?= Html:: dropDownList ('text', null, ['1'=>'Male','2'=>'Female'], array('class' => 'form-control')) ?>

	<?= Html::label('Age') ?>
	<?= Html:: dropDownList ('text', null, ['1'=>'15-25','2'=>'26-35','3'=>'36-45','4'=>'46-55'], array('class' => 'form-control')) ?>

	<?= Html::label('Work Information') ?>
	<?= Html::input('text', 'null', '', array('class' => 'form-control')) ?>

	<?= Html::label('Job Title') ?>
	<?= Html::input('text', 'null', '', array('class' => 'form-control')) ?>

	<?= Html::label('Company/Organization') ?>
	<?= Html::input('text', 'null', '', array('class' => 'form-control')) ?>


    <div class="form-group">
    <?= Html::a('Daftar', ['metodebayar'], ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
