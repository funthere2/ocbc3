<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FutsalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fun Futsal';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="futsal-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Register Futsal', ['create'], ['class' => 'btn btn-success']) ?>

        <?= Html::a('Semua Peserta', ['/pesertafutsal/index'], ['class' => 'btn btn-primary']) ?>

        <?php
        $urlexcel = array_filter(Yii::$app->getRequest()->get());
        $urlexcel[0] = 'export-excel';
        unset($urlexcel['page']);
        ?>

        <?= Html::a('Export Excel ', $urlexcel, ['class' => 'btn btn-info', 'target' => '_blank']); ?>

    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=> Yii::$app->params['summary'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'Kode_Futsal',
            'Nama_Team',
            'Nama_Manager_Team',
            'Email_Perwakilan:email',
            'No_HP',
             'Keterangan:ntext',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
