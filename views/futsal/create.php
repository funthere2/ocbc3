<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Futsal */

$this->title = 'Register Fun Futsalista';
//$this->params['breadcrumbs'][] = ['label' => 'Futsals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="futsal-create">

   
    <?= $this->render('_form', [
        'model' => $model,
        'details' => $details,
    ]) ?>

</div>
