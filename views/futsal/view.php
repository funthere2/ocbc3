<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Futsal */

$this->title = "Team : ".$model->Nama_Team;
//$this->params['breadcrumbs'][] = ['label' => 'Futsals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="futsal-view">



    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kode_Futsal], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kode_Futsal], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
          <?= Html::a('Export Form ', ['export-form','id'=>$model->Kode_Futsal], ['class' => 'btn btn-info', 'target' => '_blank']); ?>
         <?= Html::a('List', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kode_Futsal',
            'Nama_Team',
            'Nama_Manager_Team',
            'Email_Perwakilan:email',
            'No_HP',
            'Keterangan:ntext',
        ],
    ]) ?>
    
     <h3>Peserta</h3>
    <?=
    GridView::widget([
        'dataProvider' => $detailDataProvider,
        //  'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           // 'Kode_Peserta',
            //'Kode_Charitywalk',
            'Nama',
            'NIK',
            'Jenis_Kelamin',
             'No_HP',
             'Keterangan:email',
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
