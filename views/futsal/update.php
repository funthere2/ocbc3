<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Futsal */

$this->title = 'Update Futsal: ' . $model->Kode_Futsal;
$this->params['breadcrumbs'][] = ['label' => 'Futsals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Kode_Futsal, 'url' => ['view', 'id' => $model->Kode_Futsal]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="futsal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
          'details' => $details,
    ]) ?>

</div>
