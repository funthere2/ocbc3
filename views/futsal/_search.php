<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FutsalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="futsal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Kode_Futsal') ?>

    <?= $form->field($model, 'Nama_Team') ?>

    <?= $form->field($model, 'Nama_Manager_Team') ?>

    <?= $form->field($model, 'Email_Perwakilan') ?>

    <?= $form->field($model, 'No_HP') ?>

    <?php // echo $form->field($model, 'Keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
