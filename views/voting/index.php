<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VotingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Voting';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voting-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <p>
        
          <?php
        $urlexcel = array_filter(Yii::$app->getRequest()->get());
        $urlexcel[0] = 'export-excel';
        unset($urlexcel['page']);
        ?>

        <?= Html::a('Export Excel ', $urlexcel, ['class' => 'btn btn-info', 'target' => '_blank']); ?>
    </p>

  
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=> Yii::$app->params['summary'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'Kode_Voting',
             'created:datetime',
           // 'Nama',
           
            //'Email:email',
             'Pilihan',
            'field1',
             'field2',
            
            // 'updated',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
