<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Voting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="voting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php //$form->field($model, 'Nama')->textInput(['maxlength' => true]) ?>
    
    <?php  //$form->field($model, 'Email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Pilihan')->textInput() ?>
    
      <?= $form->field($model, 'field1')->textInput() ?>
    
      <?= $form->field($model, 'field2')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Vote' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
