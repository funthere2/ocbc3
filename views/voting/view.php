<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Voting */

$this->title = "Voting : ".$model->Nama;
//$this->params['breadcrumbs'][] = ['label' => 'Votings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voting-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Kode_Voting], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->Kode_Voting], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
             <?= Html::a('List Voting', ['list'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Kode_Voting',
          //  'Nama',
            'Pilihan',
           // 'Email:email',
            'field1',
            'field2',
            'created:datetime',
            'updated:datetime',
        ],
    ]) ?>

</div>
