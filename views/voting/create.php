<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Voting */

$this->title = 'Ayo Pilih Foto Favorit Kamu!';
//$this->params['breadcrumbs'][] = ['label' => 'Votings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voting-create">

    <div class="alert alert-success alert-dismissable">
        <h3>Masukkan nomor foto favoritmu pada kolom di bawah ini sebagai bentuk partisipasimu <br>
dalam rangkaian kegiatan 49 Tahun Indosat Ooredoo</h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
